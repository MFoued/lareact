<?php

namespace App\Http\Controllers\Api;

use App\Mail\ResetPassword;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\ResetsPasswords;

class AuthController extends BaseController
{
    
     use ResetsPasswords;

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $email = $request->input('email');
        $password = $request->input('password');

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            $user = Auth::user();
            $success['token'] = 'Bearer ' . $user->createToken('MyApp')->accessToken;
            $user->role;

            $success['user'] = $user;

            return $this->sendResponse($success, 'User logged in successfully.');
        }

        return $this->sendError('Wrong Credentials.');
    }


    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required', 
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');

        $user = User::where('email', $email) -> first();

        if ($user) {
            return $this->sendError('This email address is already taken. Please try another.');
        }

        User::create([
        	'name' => $name,
        	'email' => $email,
        	'password' => bcrypt($password)
        ]); 

        return $this->sendResponse([], 'User created successfully.');
    }



    public function sendResetEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $email = $request->input('email');
        
        $response = Password::sendResetLink(['email' => $email], function (Message $message) {
            $message->subject($this->getEmailSubject());
	    });

	    switch ($response) {
	        case Password::RESET_LINK_SENT:
	        	$success = [];
                return $this->sendResponse($success, 'We have e-mailed your password reset link!');
	        case Password::INVALID_USER:
	            return $this->sendError('We can\'t find a user with that e-mail address.');
	    }
    }

    public function reset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );
        
        if ($response == Password::PASSWORD_RESET) {
            $success = [];
        	return $this->sendResponse($success, 'User password has been successfully reset.');
        } else {
            return $this->sendError(trans($response));
        }
    }

    public function logout(Request $request)
    {
        if (Auth::check()) {
            Auth::user()->oauthAcessTokens()->delete();
            return $this->sendResponse([], 'User logged out successfully.');
        }
    }
}