<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function() {
	Route::get('test', function () {
	    return response()->json(['success' => true]);
	});

	Route::post('authenticate', 'Api\AuthController@authenticate');
	Route::post('register', 'Api\AuthController@register');
	Route::post('password/email', 'Api\AuthController@sendResetEmail');
	Route::post('password/reset', 'Api\AuthController@reset');
	
	Route::middleware('auth:api')->group(function () {
	    Route::post('logout', 'Api\AuthController@logout');
	});
});



